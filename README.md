**This repo holds the vbmeta image file to disable verity on Xiaomi Redmi Note 7 PRO (Violet).**

**Instructions:-**

(*) Using fastboot command type : `fastboot --disable-verity --disable-verification flash vbmeta vbmeta.img`